from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("<int:month>", views.month_num),
    path("<str:month>", views.param_month, name="month-name")
]
