from django.shortcuts import render
from django.http import Http404, HttpResponseNotFound, HttpResponseRedirect
from django.urls import reverse

# Dictionary list for monthly
monthly_list = {
    "january": "You understand about this lecture!",
    "february": "Started with requirement tools!",
    "march": "Understanding foundation about your project!",
    "april": "Prerequisite your python tools!",
    "may": "Using IDE and python module!",
    "june": "Installing django project in own local computer",
    "july": "Understanding about Django framework!",
    "august": "learning about urls, views, template",
    "september": "understanding about startproject and startapp using django-admin command",
    "october": "creating own first urls and connected to views!",
    "november": "understanding about different structure urls between project urls and apps urls",
    "december": None
}


def index(request):
    list_items = ""
    months = list(monthly_list.keys())

    return render(request, "challenges/index.html",{
        "months": months,
    })

    # for listed in listing:
    #     capitalized = listed.capitalize()
    #     path_listed = reverse("month-name", args=[listed])
    #     list_items += f"<li><a href=\"{path_listed}\">{capitalized}</a></li>"

    # response_data = f"<ul>{list_items}</ul>"

    # return HttpResponse(response_data)


def month_num(request, month):
    forwarder = list(monthly_list.keys())

    if month > len(forwarder):
        return HttpResponseNotFound("<h1>Your Paramater is incorrect!</h1>")

    forward_list = forwarder[month - 1]
    redirect_path = reverse("month-name", args=[forward_list])
    return HttpResponseRedirect(redirect_path)


def param_month(request, month):
    try:
        res_text = monthly_list[month]
        return render(request, "challenges/challenge.html", {
            "text": res_text
        })
    except:
        return Http404()
